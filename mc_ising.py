from matplotlib import *
import numpy as np
import scipy

class mc_ising:
    def __init__(self, Lx, Ly, J, H):
        self.Lx = Lx
        self.Ly = Ly
        self.J = J
        self.H = H
        self.sigma = np.ones((Lx, Ly))
        self.hist_m = []
        self.hist_E = []
        self.size = None
        if Lx == Ly: self.size = Lx

    def mcflip_sublattice(self, a, b):
        localH = self.H + self.J * (
            self.sigma[2+a: :2, 1+b:-1:2] +
            self.sigma[a:-2:2,1+b:-1:2]+
            self.sigma[1+a:-1:2,2+b::2]+
            self.sigma[1+a:-1:2,b:-2:2]
            )
        m = np.tanh(localH)
        p = (m+1)/2.

        self.sigma[1+a:-1:2,1+b:-1:2] = 2*(np.random.rand(self.Lx//2-1, self.Ly//2-1) < p) - 1
            
    def mcflip(self):
        for x in [0, 1]:
            for y in [0, 1]:
                self.mcflip_sublattice(x, y)

    def energy(self):
        return (
            -self.J * np.sum(self.sigma[:-1,:] * self.sigma[1:,:])
            -self.J * np.sum(self.sigma[:,:-1] * self.sigma[:,1:])
            -self.H * np.sum(self.sigma)
            )

    def magnetisation(self):
        return np.mean(self.sigma[1:-1, 1:-1])

    def __str__(self):
        return "ising_2d(%d, %d, %f, %f)" % (self.Lx, self.Ly, self.J, self.H)

    def run(self, steps, sample_steps):
        print ( "Running " + str(self))
        for t in range(steps):
            self.mcflip()
            if t % sample_steps == 0:
                self.hist_m.append(self.magnetisation())
                self.hist_E.append(self.energy())
