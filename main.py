from matplotlib import *
import numpy as np
import scipy
import cProfile
from mc_ising import *


        
sizes = [10, 16]
Js = [0.5, 0.9, 1.1]
Hs = [0., 0.1]
sims = []
energies = []
energies_var = []
mags = []
mags_var = []
    
for (s, J, H) in [(s, J, H) for s in sizes for J in Js for H in Hs]:
    sim = mc_ising(s, s, J, H)
    sim.run(1000, 10)
    sims.append(sim)
    energies.append(np.mean(sim.hist_E))
    energies_var.append(np.var(sim.hist_E))
    mags.append(np.mean(sim.hist_m))
    mags_var.append(np.var(sim.hist_m))


def filter_sims(sims, condition):
    return [sim for sim in sims if condition.items() <= vars(sim).items()]

def get_measurements(sims, key):
    data_key = 'hist_' + key
    data = [getattr(s, data_key) for s in sims]
    return (map(np.mean, data), map(np.var, data))

    
figure()
for s in sizes:
    for H in Hs:
        condition = {'H':H, 'size':s}
        sims_f = filter_sims(sims, condition)
        get_measurements(sims_f, 'E')
            
a = mc_ising(10, 10, 1.0, 0.0)
print(a.energy())
print(a.Lx)
